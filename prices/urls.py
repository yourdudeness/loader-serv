from django.urls import path

from . import views

urlpatterns = [
    path('', views.PricesView.as_view(), name='prices'),
]
