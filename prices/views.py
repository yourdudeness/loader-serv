from django.views import generic

from main.models import Performer
from .models import HouseMovingService, OfficeMovingService, CottageMovingService, LoaderService, \
    HandymenService, DemolitionWorkService, RiggingService, GarbageRemovalService, \
    SnowRemovalService, SpecialMachineryRentalService


def get_service_categories():
    return [
        {
            'name': 'Квартирный переезд',
            'services': HouseMovingService.objects.all()
        },
        {
            'name': 'Офисный переезд',
            'services': OfficeMovingService.objects.all()
        },
        {
            'name': 'Дачный переезд',
            'services': CottageMovingService.objects.all()
        },
        {
            'name': 'Услуги грузчиков',
            'services': LoaderService.objects.all()
        },
        {
            'name': 'Разнорабочие',
            'services': HandymenService.objects.all()
        },
        {
            'name': 'Демонтажные работы',
            'services': DemolitionWorkService.objects.all()
        },
        {
            'name': 'Такелажные работы',
            'services': RiggingService.objects.all()
        },
        {
            'name': 'Вывоз мусора',
            'services': GarbageRemovalService.objects.all()
        },
        {
            'name': 'Уборка снега',
            'services': SnowRemovalService.objects.all()
        },
        {
            'name': 'Аренда спецтехники',
            'services': SpecialMachineryRentalService.objects.all()
        }
    ]


class PricesView(generic.TemplateView):
    template_name = 'prices.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'service_categories': get_service_categories(),
            'performers': Performer.objects.all(),
        })
        return context
