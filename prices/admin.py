from django.contrib import admin

from .models import HouseMovingService, OfficeMovingService, CottageMovingService, LoaderService, \
    HandymenService, DemolitionWorkService, RiggingService, GarbageRemovalService, \
    SnowRemovalService, SpecialMachineryRentalService


class ServiceAdmin(admin.ModelAdmin):
    pass


@admin.register(HouseMovingService)
class HouseMovingServiceAdmin(ServiceAdmin):
    pass


@admin.register(OfficeMovingService)
class OfficeMovingServiceAdmin(ServiceAdmin):
    pass


@admin.register(CottageMovingService)
class CottageMovingServiceAdmin(ServiceAdmin):
    pass


@admin.register(LoaderService)
class LoaderServiceAdmin(ServiceAdmin):
    pass


@admin.register(HandymenService)
class HandymenServiceAdmin(ServiceAdmin):
    pass


@admin.register(DemolitionWorkService)
class DemolitionWorkService(ServiceAdmin):
    pass


@admin.register(RiggingService)
class RiggingServiceAdmin(ServiceAdmin):
    pass


@admin.register(GarbageRemovalService)
class GarbageRemovalServiceAdmin(ServiceAdmin):
    pass


@admin.register(SnowRemovalService)
class SnowRemovalServiceAdmin(ServiceAdmin):
    pass


@admin.register(SpecialMachineryRentalService)
class SpecialMachineryRentalServiceAdmin(ServiceAdmin):
    pass
