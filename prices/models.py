from django.db import models
from django.db.models import Min


class ServiceManager(models.Manager):

    def min_price(self):
        aggregate = self.all().aggregate(Min('price'))
        return aggregate['price__min']


class ServiceAbstract(models.Model):
    name = models.CharField(verbose_name='Название', max_length=256)
    price = models.PositiveIntegerField(verbose_name='Цена', help_text='от')
    price_stock = models.PositiveIntegerField(verbose_name='Цена по акции', help_text='от')

    objects = ServiceManager()

    def __str__(self):
        return self.name

    class Meta:
        abstract = True


class HouseMovingService(ServiceAbstract):
    class Meta:
        verbose_name = 'Услуга'
        verbose_name_plural = 'Квартирный переезд'


class OfficeMovingService(ServiceAbstract):
    class Meta:
        verbose_name = 'Услуга'
        verbose_name_plural = 'Офисный переезд'


class CottageMovingService(ServiceAbstract):
    class Meta:
        verbose_name = 'Услуга'
        verbose_name_plural = 'Дачный переезд'


class LoaderService(ServiceAbstract):
    class Meta:
        verbose_name = 'Услуга'
        verbose_name_plural = 'Услуги грузчиков'


class HandymenService(ServiceAbstract):
    class Meta:
        verbose_name = 'Услуга'
        verbose_name_plural = 'Разнорабочие'


class DemolitionWorkService(ServiceAbstract):
    class Meta:
        verbose_name = 'Услуга'
        verbose_name_plural = 'Демонтажные работы'


class RiggingService(ServiceAbstract):
    class Meta:
        verbose_name = 'Услуга'
        verbose_name_plural = 'Такелажные работы'


class GarbageRemovalService(ServiceAbstract):
    class Meta:
        verbose_name = 'Услуга'
        verbose_name_plural = 'Вывоз мусора'


class SnowRemovalService(ServiceAbstract):
    class Meta:
        verbose_name = 'Услуга'
        verbose_name_plural = 'Уборка снега'


class SpecialMachineryRentalService(ServiceAbstract):
    class Meta:
        verbose_name = 'Услуга'
        verbose_name_plural = 'Аренда спецтехники'
