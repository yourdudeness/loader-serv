$(window).on('load', function () {
  var markerPath = $('#map').attr('data-marker');
  var map = document.querySelector('.actual__vacancy-map, .office__map');

  var onIntersection = function onIntersection(entries) {
    var _iteratorNormalCompletion = true;
    var _didIteratorError = false;
    var _iteratorError = undefined;

    try {
      for (var _iterator = entries[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
        var entry = _step.value;

        if (entry.intersectionRatio > 0) {
          ymaps.ready(mapsInit);
          observer.unobserve(entry.target);
        }
      }
    } catch (err) {
      _didIteratorError = true;
      _iteratorError = err;
    } finally {
      try {
        if (!_iteratorNormalCompletion && _iterator.return != null) {
          _iterator.return();
        }
      } finally {
        if (_didIteratorError) {
          throw _iteratorError;
        }
      }
    }
  };

  if ($('#map').length) {
    var observer = new IntersectionObserver(onIntersection);
    observer.observe(map);
  }

  function mapsInit() {
    myMap = new ymaps.Map('map', {
      center: [56.21845656852234,43.86759199999999],
      zoom: 13,
      controls: ['zoomControl']
    });
    var Place = new ymaps.Placemark([56.21845656852234,43.86759199999999], {
      balloonContent: ''
    }, {
        hasBalloon: false,
        iconLayout: 'default#image',
        iconImageHref: markerPath,
        iconImageSize: [38, 50],
        iconImageOffset: [-19, -50],
      });
    myMap.geoObjects.add(Place);
    myMap.behaviors.disable('scrollZoom');
  };
});
