$(window).on('load', function () {
	$('.comment__wrapper').slick({
		adaptiveHeight: true,
		infinite: true,
		arrows: true,
		nextArrow: '<button class="slick-next" aria-hidden="true"></button>',
		prevArrow: '<button class="slick-prev" aria-hidden="true"></button>',
		slidesToShow: 4,
		slidesToScroll: 1,
		dots: false,
		dotsClass: 'custom-dots dots--comment',

		responsive: [{
				breakpoint: 1200,
				settings: {
					slidesToShow: 3,
				}
			},
			{
				breakpoint: 900,
				settings: {
					slidesToShow: 2,
				}
			},
			{
				breakpoint: 550,
				settings: {
					slidesToShow: 1,
				}
			}
		]
	});

	//при переходе с карточки услуги на главной на страницу услуг открывать нужный таб 
	let card = $('services__item');
	let priceButton = $('.price__item');
	let priceTable = $('.price__table');
	let priceDesc = $('.info__elem');

	if (document.location.hash) {
		let curTab = document.location.hash.toString().slice(1);
		priceButton.removeClass('price__item--active');
		priceTable.removeClass('price__table--active');
		priceDesc.removeClass('info__elem--active');

		$('button[data-tab ="' + curTab + '"]').addClass('price__item--active');
		$('#' + curTab + '').addClass('price__table--active');
		$('.info__elem[data-number="' + curTab + '"]').addClass('info__elem--active');
		document.location.hash = document.location.hash.split('#')[0];
	}

	$('.gratitude__slider').slick({
		adaptiveHeight: true,
		infinite: true,
		dots: false,
		dotsClass: 'custom-dots dots--gratitude',
		arrows: true,
		nextArrow: '<button class="slick-next" aria-hidden="true"></button>',
		prevArrow: '<button class="slick-prev" aria-hidden="true"></button>',
		slidesToShow: 7,
		slidesToScroll: 1,
		responsive: [{
				breakpoint: 1255,
				settings: {
					slidesToShow: 5,
				}
			},
			{
				breakpoint: 900,
				settings: {
					slidesToShow: 4,
				}
			},
			{
				breakpoint: 750,
				settings: {
					slidesToShow: 3,
				}
			},
			{
				breakpoint: 620,
				settings: {
					slidesToShow: 2,
				}
			},
			{
				breakpoint: 480,
				settings: {
					slidesToShow: 1,
				}
			},
		]
	});
});

//Добавить благодарственному письму кнопку закрытия, стрелки, изменить размер
$('[data-fancybox="gallery"]').fancybox({
	smallBtn: true,
	arrows: false,
	slideClass: "gratitude__fancybox-img",
	autoScale: true,
	autoDimensions: false,
	width: 500,
	arrows: false,
	afterLoad: function (instance, current) {
		if (instance.group.length > 1 && current.$content) {
			current.$content.append('<button class="fancybox-button fancybox-button--arrow_right next" data-fancybox-next><i class="fa fa-angle-right" aria-hidden="true"></i></button><button data-fancybox-prev class="fancybox-button fancybox-button--arrow_left prev"><i class="fa fa-angle-left" aria-hidden="true"></i></button>');
		}
	}
});


//Спойлер с вопросами
$('.question__block').find('.question__desc').hide();

$('.question__block').on('click', function () {
	if ($(this).hasClass('question__block--active')) {
		$(this).removeClass('question__block--active');
		$(this).find('.question__desc').slideUp(200);
	} else {
		$(this).addClass('question__block--active');
		$(this).find('.question__desc').slideDown(200);
	}

});


//Вакансии
if ($('.actual__vacancy').length > 0) {

	const vacancyBtn = $('.actual__vacancy-item');

	vacancyBtn.each(function () {
		let tabId = $(this).attr('data-tab');
		let tab = $('#' + tabId).html();
		$(this).parents('.actual__vacancy-available-elem').find('.actual__vacancy-available-elem-tab').append(tab);
	});

	vacancyBtn.on('click', function () {
		vacancyBtn.removeClass('actual__vacancy-item--active');
		$(this).addClass('actual__vacancy-item--active');

		$('.actual__vacancy-tab').removeClass('tab--active');
		let currentTab = $(this).attr('data-tab');
		$('#' + currentTab).addClass('tab--active');

		$('.actual__vacancy-available-elem-tab').removeClass('tab--active');
		$(this).parents('.actual__vacancy-available-elem').find('.actual__vacancy-available-elem-tab').addClass('tab--active');
	});

	//добавляем атрибут на кнопку
	profileBtn = $('.actual__vacancy-tab .actual__vacancy-button');
	profileBtn.each(function () {
		let tabId = $(this).parents('.actual__vacancy-tab').attr('id');
		let vacancy = $('.actual__vacancy-item[data-tab="' + tabId + '"]').html();
		$(this).attr('data-vacancy', vacancy);
	});
	profileBtnMobile = $('.actual__vacancy-available-elem .actual__vacancy-button');
	profileBtnMobile.each(function () {
		let vacancy = $(this).parents('.actual__vacancy-available-elem').find('.actual__vacancy-item').html();
		$(this).attr('data-vacancy', vacancy);
	});

	profileBtn.add(profileBtnMobile).on('click', function () {
		$('.profile__form-input--vacancy').val($(this).attr('data-vacancy'));
	});
}

// Прайс услуг
if ($('.price').length > 0) {

	const priceBtn = $('.price__item');
	const priceTab = $('.price__table');
	const priceDrop = $('.price__service');
	const priceDescription = $('.info__elem');

	priceBtn.on('click', function () {

		let priceTabId = $(this).attr('data-tab');
		priceBtn.removeClass('price__item--active');
		$(this).addClass('price__item--active');
		priceTab.removeClass('price__table--active');
		$('#' + priceTabId).addClass('price__table--active');
		priceDescription.removeClass('info__elem--active');
		$('.info__elem[data-number="' + priceTabId + '"]').addClass('info__elem--active');

		//tablet and mobile

	});

	if ($(window).width() <= 1300) {

		$('.price__item').parents('.price__available-elem').addClass('elem--active');
		$('.price__item').parents('.price__available-elem').find('.price__note').slideDown(200);
		$('.price__item').parents('.price__available-elem').find('.price__table').slideDown(200);
	}


	//Выпадающий прайс в мобильном меню
	priceDrop.parents('.price__catalog').find('.price__service-cost').slideUp(0);
	priceDrop.on('click', function () {

		if ($(this).hasClass('price__service--active')) {
			$(this).removeClass('price__service--active');
			$(this).parents('.price__catalog').find('.price__service-cost').slideUp(200);
		} else {
			priceDrop.removeClass('price__service--active');
			$(this).addClass('price__service--active');
			$(this).parents('.price__catalog').find('.price__service-cost').slideDown(200);
		}
	});
}

//Вызов меню
$('.header__burger').click(function () {
	bodyScrollLock.disableBodyScroll(document.querySelector('.header__wrapper'));
	$('.header__burger').fadeOut(100);
	$('.header__wrapper').fadeIn(100);
	$('.header__nav-close').addClass('close--active');
});

$('.header__nav-close').click(function () {
	$(this).removeClass('close--active');
	bodyScrollLock.enableBodyScroll(document.querySelector('.header__wrapper'));
	$('.header__wrapper').fadeOut(100);
	$('.header__burger').fadeIn(100);
});


//Очистка форм после закрытия модальника
function clearForm() {
	$('.form--feedback').trigger('reset');
	$('.form--feedback input').removeClass('field--invalid');
	$('.form--feedback .select-styled').html('Гражданство');
	$('.modal__form-input-check, .modal__form-input-error').hide();
	$('.request__form-wrap, .order__form-wrap, .profile__form-cover, .profile__form-birth, .profile__form-text, .write-director__form').removeClass('invalid');
}

const modal = $('.fancybox-content');
$('.footer__link--politics, .request__politic--link, .banner__request, .header__order, .actual__vacancy-button, .consultation__link, .accuracy__link, .interview__link, .call__link, .about__write-director, .write-director__politic-link').fancybox({
	toolbar: false,
	smallBtn: true,
	touch: false,
	autoFocus: false,
	afterLoad: function () {
		bodyScrollLock.disableBodyScroll(document.querySelector('.fancybox-slide'));
		clearForm();
	},
	beforeClose: function () {
		bodyScrollLock.enableBodyScroll(document.querySelector('.fancybox-slide'));
	},
});


//Маски ввода для телефона и даты рождения
var phone = document.querySelectorAll('.phone');
phone.forEach(function (element) {
	var phoneMask = IMask(
		element, {
			mask: '+{7}(000)000-00-00',
			placeholderChar: '_',
			lazy: false,
			overwrite: false,
		});
});

var birthDate = document.querySelectorAll('.data');
birthDate.forEach(function (element) {
	var birthMask = IMask(
		element, {
			mask: Date,
			min: new Date(1960, 0, 1),
			max: new Date(2020, 0, 1),
			placeholderChar: '_',
			lazy: false,
			overwrite: false,
		});
});

//Отправка с форм
var feedbackForm = $('.form--feedback');
//Check validate input
feedbackForm.on('submit', function (e) {
	e.preventDefault();

	var data = {
		csrfmiddlewaretoken: $(this).find('input[name="csrfmiddlewaretoken"]').val()
	};

	const hiddenField = $(this).find('.sec-field');
	var Err = true;
	var ErrPhone = false;
	var ErrDate = false;

	//check empty field
	$(this).find('.req-input').each(function () {
		$(this).parents('.request__form-wrap, .order__form-wrap, .profile__form-cover, .profile__form-birth, .profile__form-text, .write-director__form').removeClass('invalid');
		if ($(this).val().trim() === '') {
			$(this).parents('.request__form-wrap, .order__form-wrap, .profile__form-cover, .profile__form-birth, .profile__form-text, .write-director__form').addClass('invalid');
		}
	});

	//check full phone
	let phoneField = $(this).find('.phone');
	phoneField.each(function () {
		let countNum = 0;
		let str = $(this).val();

		for (var i = 0; i < str.length; i++) {
			if (!isNaN(str[i]) && str[i] !== ' ') {
				countNum++;
			}
		}
		if (countNum == 11) {
			ErrPhone = false;
		} else {
			ErrPhone = true;
			$(this).parents('.request__form-wrap, .order__form-wrap, .profile__form-cover, .profile__form-birth, .profile__form-text, .write-director__form').addClass('invalid');
		}
	});

	//check full date
	let dateField = $(this).find('.data');
	dateField.each(function () {
		let countNum = 0;
		let str = $(this).val();

		for (var i = 0; i < str.length; i++) {
			if (!isNaN(str[i]) && str[i] !== ' ') {
				countNum++;
			}
		}
		if (countNum == 8) {
			ErrDate = false;
		} else {
			ErrDate = true;
			$(this).parents('.request__form-wrap, .order__form-wrap, .profile__form-cover, .profile__form-birth, .profile__form-text').addClass('invalid');
		}
	});

	//select modal thank-you
	var modalThankYou = ($(this).hasClass('profile__form') || $(this).hasClass('write-director__form')) ? '#send-2' : '#send';

	//-- antispam
	hiddenField.each(function () {
		if ($(this).val() === '') {
			Err = false;
		} else {
			Err = true;
			return false;
		}
	});
	//--

	// create array of data
	$(this).find('.form-input').each(function () {
		data[$(this).attr('name')] = $(this).val();
	});


	var countEmpty = $(this).find('.invalid').length;

	if (countEmpty > 0 || Err === true || ErrPhone === true || ErrDate === true) {
		return false
	} else {

		console.log('send message');

		//=============== ajax ===================
		var url = $(this).attr('action');
		$.ajax(url, {
			method: 'post',
			data: data,
			success: function (response) {

				//close current modal
				$.fancybox.close({
					src: '.fancybox-content',
					type: 'inline',
				});
				//open modal thanks
				$.fancybox.open({
					src: modalThankYou,
					type: 'inline',
					opts: {
						beforeClose: function () {
							bodyScrollLock.enableBodyScroll(document.querySelector('.fancybox-slide'));
							clearForm();
						}
					}
				}, {
					autoFocus: false,
					touch: false,
				});
			}
		});

		//=============== ajax ===================

	}

});