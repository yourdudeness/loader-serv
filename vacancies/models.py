from django.db import models
from ckeditor.fields import RichTextField
from imagekit.models import ProcessedImageField
from utils import upload_to


class Vacancy(models.Model):
    title = models.CharField(verbose_name='Название', max_length=256)
    salary = models.CharField(verbose_name='Уровень зарплаты', max_length=256)
    experience = models.CharField(verbose_name='Опыт работы', max_length=256)
    description = RichTextField(verbose_name='Описание')

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Вакансия'
        verbose_name_plural = 'Вакансии'


class PerformerReview(models.Model):
    author = models.CharField(verbose_name='Автор', max_length=256)
    photo = ProcessedImageField(verbose_name='Фото', upload_to=upload_to, format='JPEG', options={'quality': 75})
    subtitle = models.CharField(verbose_name='Подзаголовок', help_text='Возраст, должность', max_length=256)
    text = RichTextField(verbose_name='Текст')

    def __str__(self):
        return self.author

    class Meta:
        verbose_name = 'Отзыв исполнителя'
        verbose_name_plural = 'Отзывы исполнителей'
