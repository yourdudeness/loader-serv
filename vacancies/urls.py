from django.urls import path

from . import views

urlpatterns = [
    path('', views.VacanciesView.as_view(), name='vacancies'),
]
