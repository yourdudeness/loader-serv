from django.contrib import admin

from .models import Vacancy, PerformerReview


@admin.register(Vacancy)
class VacancyAdmin(admin.ModelAdmin):
    pass


@admin.register(PerformerReview)
class PerformerReviewAdmin(admin.ModelAdmin):
    pass
