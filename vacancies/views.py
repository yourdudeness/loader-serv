from datetime import date

from django.views import generic
from main.models import Client
from client_requests.forms import VacancyResponseForm

from .models import Vacancy, PerformerReview


class VacanciesView(generic.ListView):
    model = Vacancy
    template_name = 'vacancies.html'

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(object_list=object_list, **kwargs)
        context.update({
            'date': date.today(),
            'performer_reviews': PerformerReview.objects.all(),
            'clients': Client.objects.all(),
            'form': VacancyResponseForm()
        })
        return context
