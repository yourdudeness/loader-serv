from django.urls import reverse


class MenuItem:
    request = None

    def __init__(self, label, url, aliases=None):
        self.label = label
        self.url = url
        self.aliases = aliases or []

    def __str__(self):
        return self.label

    def set_request(self, request):
        self.request = request

    @property
    def active(self):
        assert self.request is not None
        return self.request.path in [self.url, *self.aliases]


class BaseMenu:
    order_items = None

    def __init__(self, request):
        self.request = request
        item_names = self.get_item_names()
        items = []
        for item_name in item_names:
            item = getattr(self, item_name)
            item.set_request(request)
            items.append(item)
        self.items = items

    def get_item_names(self):
        if self.order_items:
            return self.order_items
        return [item for item in dir(self) if isinstance(getattr(self, item), MenuItem)]


class MainMenu(BaseMenu):
    prices = MenuItem('Стоимость услуг', reverse('moving'))
    company = MenuItem('Компания', reverse('company'), aliases=[reverse('company-private')])
    vacancies = MenuItem('Вакансии', reverse('vacancies'))
    contacts = MenuItem('Контакты', reverse('contacts'))

    order_items = [
        'prices', 'company', 'vacancies', 'contacts'
    ]
