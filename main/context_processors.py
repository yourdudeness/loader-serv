from datetime import date

from .menus import MainMenu


def current_year(request):
    return {
        'current_year': date.today().year
    }


def menu(request):
    return {
        'main_menu': MainMenu(request)
    }
