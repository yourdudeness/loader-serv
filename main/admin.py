from django.contrib import admin
from django.contrib.auth.models import User, Group
from solo.admin import SingletonModelAdmin
from utils import MaxCountObjectsMixin

from .models import Contacts, Documents, Client, ThankLetter, Review, FAQ, Performer, Partner, CompanyData, CompanyText, CompanyTextPrivate,ServicesDescription,MetaTag


@admin.register(Contacts)
class ContactsAdmin(SingletonModelAdmin):
    pass

@admin.register(CompanyData)
class CompanyData(SingletonModelAdmin):
    pass

@admin.register(CompanyText)
class CompanyText(SingletonModelAdmin):
    pass

@admin.register(CompanyTextPrivate)
class CompanyTextPrivate(SingletonModelAdmin):
    pass

@admin.register(ServicesDescription)
class ServicesDescriptionAdmin(SingletonModelAdmin):
    pass

@admin.register(MetaTag)
class MetaTagAdmin(SingletonModelAdmin):
    pass

@admin.register(Documents)
class DocumentsAdmin(SingletonModelAdmin):
    pass


@admin.register(Client)
class ClientAdmin(admin.ModelAdmin):
    pass


@admin.register(ThankLetter)
class ThankLetterAdmin(admin.ModelAdmin):
    pass


@admin.register(Review)
class ReviewAdmin(admin.ModelAdmin):
    pass


@admin.register(FAQ)
class FAQAdmin(admin.ModelAdmin):
    pass


@admin.register(Performer)
class PerformerAdmin(admin.ModelAdmin):
    pass


@admin.register(Partner)
class PartnerAdmin(MaxCountObjectsMixin):
    max_objects = 4


admin.site.unregister(User)
admin.site.unregister(Group)
