from django import template
from django.template.defaultfilters import stringfilter

register = template.Library()


@register.filter
@stringfilter
def phoneformat(value):
    return '(%s) %s %s-%s' % (value[2:5], value[5:8], value[8:10], value[10:12])
