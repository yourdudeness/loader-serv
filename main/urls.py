from django.urls import path, include

from . import views


urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),
    path('company/', views.AboutCompanyView.as_view(), name='company'),
    path('company-private/', views.AboutCompanyPrivateView.as_view(), name='company-private'),
    path('contacts/', views.ContactsView.as_view(), name='contacts'),
    path('vacancies/', include('vacancies.urls')),
    path('uslugi/', include('prices.urls')),
    path('ajax/', include('client_requests.urls')),
    path('pereezdy/', views.MovingView.as_view(), name='moving'),
    path('gruzchiki/', views.MoverView.as_view(), name ='mover'),
    path('raznorabochie/', views.HandyMenView.as_view(), name = 'handymen'),
    path('demontazh/', views.DismantlingView.as_view(), name = 'dismalting'),
    path('perevozka-gruzov/', views.ShippingView.as_view(), name = 'shipping')    
]
