from django.db import models
from solo.models import SingletonModel
from imagekit.models import ProcessedImageField
from ckeditor.fields import RichTextField
from utils import validate_phone, upload_to


class Contacts(SingletonModel):
    phone = models.CharField(verbose_name='Телефон', max_length=12, validators=[validate_phone])
    phone2 = models.CharField(verbose_name='Второй телефон', max_length=12, validators=[validate_phone])
    email = models.EmailField(verbose_name='E-mail')
    vk_url = models.CharField(verbose_name='VK', max_length=256)
    instagram_url = models.CharField(verbose_name='Instagram', max_length=256)
    youtube_url = models.CharField(verbose_name='YouTube', max_length=256)
    adress = models.CharField(verbose_name='Фактический адрес', max_length=256)

    def __str__(self):
        return 'Контакты'

    class Meta:
        verbose_name = 'Контакты'
        verbose_name_plural = 'Контакты'


class Documents(SingletonModel):
    contract_doc = models.FileField(verbose_name='Договор (doc.)', upload_to=upload_to, blank=True)
    contract_pdf = models.FileField(verbose_name='Договор (pdf.)', upload_to=upload_to, blank=True)
    act_doc = models.FileField(verbose_name='Акт (doc.)', upload_to=upload_to, blank=True)
    act_pdf = models.FileField(verbose_name='Акт (pdf.)', upload_to=upload_to, blank=True)
    details_doc = models.FileField(verbose_name='Реквизиты (doc.)', upload_to=upload_to, blank=True)
    details_pdf = models.FileField(verbose_name='Реквизиты (pdf.)', upload_to=upload_to, blank=True)

    def __str__(self):
        return 'Документы'

    class Meta:
        verbose_name = 'Документы'
        verbose_name_plural = 'Документы'


class Client(models.Model):
    name = models.CharField(verbose_name='Название', max_length=256)
    logo = ProcessedImageField(verbose_name='Логотип', upload_to=upload_to, format='JPEG', options={'quality': 75})

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Клиент'
        verbose_name_plural = 'Клиенты'


class ThankLetter(models.Model):
    author = models.CharField(verbose_name='От кого', max_length=256)
    scan = ProcessedImageField(verbose_name='Скан', upload_to=upload_to, format='JPEG', options={'quality': 75})

    def __str__(self):
        return self.author

    class Meta:
        verbose_name = 'Благодарственное письмо'
        verbose_name_plural = 'Благодарственные письма'


class Review(models.Model):
    RATING_CHOICES = [
        (4, 4),
        (5, 5),
    ]

    author = models.CharField(verbose_name='Автор', max_length=256)
    phone = models.CharField(verbose_name='Телефон', help_text='Последние 4 цифры скрыть *', max_length=32)
    city = models.CharField(verbose_name='Город', max_length=256)
    text = models.TextField(verbose_name='Текст')
    rating = models.SmallIntegerField(verbose_name='Оценка', choices=RATING_CHOICES)

    def __str__(self):
        return self.author

    class Meta:
        verbose_name = 'Отзыв'
        verbose_name_plural = 'Отзывы'


class FAQ(models.Model):
    question = models.CharField(verbose_name='Вопрос', max_length=256)
    answer = RichTextField(verbose_name='Ответ')

    def __str__(self):
        return self.question

    class Meta:
        verbose_name = 'FAQ'
        verbose_name_plural = 'FAQ'


class Performer(models.Model):
    name = models.CharField(verbose_name='Имя', max_length=256)
    photo = ProcessedImageField(verbose_name='Фото', upload_to=upload_to, format='JPEG', options={'quality': 75})
    subtitle = models.CharField(verbose_name='Подзаголовок', help_text='Возраст, должность', max_length=256)
    rating = models.DecimalField(verbose_name='Средняя оценка', max_digits=2, decimal_places=1)
    rating_number = models.PositiveSmallIntegerField(verbose_name='Количество оценок')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Исполнитель'
        verbose_name_plural = 'Исполнители'


class Partner(models.Model):
    name = models.CharField(verbose_name='Название', max_length=256)
    logo = ProcessedImageField(verbose_name='Логотип', upload_to=upload_to, format='JPEG', options={'quality': 75})

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Партнер'
        verbose_name_plural = 'Партнеры'


class CompanyData(SingletonModel):
    inn = models.CharField(max_length=256, verbose_name='ИНН')
    kpp = models.CharField(max_length=256, verbose_name="КПП")
    orgn = models.CharField(max_length=256, verbose_name="ОРГН")
    bank = models.CharField(max_length=256, verbose_name="Банк")
    bik = models.CharField(max_length=256, verbose_name="БИК")
    cor = models.CharField(max_length=256, verbose_name="Кор/счет")
    rs = models.CharField(max_length=256, verbose_name="Р/С")
    adress = models.CharField(max_length=256, verbose_name="Юридический адрес")

    def __str__(self):
        return 'Реквизиты'

    class Meta:
        verbose_name = 'Реквизиты'
        verbose_name_plural = 'Реквизиты'

class CompanyText(SingletonModel):
    title = models.CharField(max_length=256, verbose_name='Заголовок')
    text = models.TextField(verbose_name='Текст')

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Компаниям'
        verbose_name_plural = 'Компаниям'

class CompanyTextPrivate(SingletonModel):
    title = models.CharField(max_length=256, verbose_name='Заголовок')
    text = models.TextField(verbose_name='Текст')

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Частным лицам'
        verbose_name_plural = 'Частным лицам'

class ServicesDescription(SingletonModel):
    title_serv = models.CharField(max_length=256, verbose_name='Заголовок для Услуг', default='SOME STRING')
    text_serv = models.TextField(verbose_name='Текст для Услуг', default='SOME STRING')
    title_move = models.CharField(max_length=256, verbose_name='Заголовок для Переезды')
    text_move = models.TextField(verbose_name='Текст для Переезды')
    title_loader = models.CharField(max_length=256, verbose_name='Заголовок для Услуги грузчиков')
    text_loader = models.TextField(verbose_name='Текст для Услуги грузчиков')
    title_auto = models.CharField(max_length=256, verbose_name='Заголовок для Автотранспорт и экспедиция грузов')
    text_auto = models.TextField(verbose_name='Текст для Автотранспорт и экспедиция грузов')
    title_worker = models.CharField(max_length=256, verbose_name='Заголовок для Услуги разнорабочих')
    text_worker = models.TextField(verbose_name='Текст для Услуги разнорабочих')
    title_dismantl = models.CharField(max_length=256, verbose_name='Заголовок для Демонтаж')
    text_dismantl = models.TextField(verbose_name='Текст для Демонтаж')

    def __str__(self):
        return 'Описание услуги'

    class Meta:
        verbose_name = 'Описание услуг'
        verbose_name_plural = 'Описание услуг'

class MetaTag(SingletonModel):
    ogtitle_ind = models.CharField(max_length=256, verbose_name='Главная-og:title', default='ГрузчикService')
    ogdescription_ind = models.CharField(max_length=256, verbose_name='Главная-og:description', default='Грузчики и услуги рабочих в Нижнем Новгороде')
    ogtype_ind = models.CharField(max_length=256, verbose_name='Главная-og:type', default='website')
    ogurl_ind = models.CharField(max_length=256, verbose_name='Главная-og:url', default='http://https://xn--b1abghfcl9beiep7c.xn--p1ai/')
    ogsitename_ind = models.CharField(max_length=256, verbose_name='Главная-og:site_name', default='ГрузчикService')

    ogtitle_per = models.CharField(max_length=256, verbose_name='Переезды-og:title', default='ГрузчикService')
    ogdescription_per = models.CharField(max_length=256, verbose_name='Переезды-og:description', default='Грузчики и услуги рабочих в Нижнем Новгороде')
    ogtype_per = models.CharField(max_length=256, verbose_name='Переезды-og:type', default='website')
    ogurl_per = models.CharField(max_length=256, verbose_name='Переезды-og:url', default='http://https://xn--b1abghfcl9beiep7c.xn--p1ai/')
    ogsitename_per = models.CharField(max_length=256, verbose_name='Переезды-og:site_name', default='ГрузчикService')

    ogtitle_mover = models.CharField(max_length=256, verbose_name='Услуги грузчиков-og:title', default='ГрузчикService')
    ogdescription_mover = models.CharField(max_length=256, verbose_name='Услуги грузчиков-og:description', default='Грузчики и услуги рабочих в Нижнем Новгороде')
    ogtype_mover = models.CharField(max_length=256, verbose_name='Услуги грузчиков-og:type', default='website')
    ogurl_mover = models.CharField(max_length=256, verbose_name='Услуги грузчиков-og:url', default='http://https://xn--b1abghfcl9beiep7c.xn--p1ai/')
    ogsitename_mover = models.CharField(max_length=256, verbose_name='Услуги грузчиков-og:site_name', default='ГрузчикService')

    ogtitle_handymen = models.CharField(max_length=256, verbose_name='Разнорабочие-og:title', default='ГрузчикService')
    ogdescription_handymen = models.CharField(max_length=256, verbose_name='Разнорабочие-og:description', default='Грузчики и услуги рабочих в Нижнем Новгороде')
    ogtype_handymen = models.CharField(max_length=256, verbose_name='Разнорабочие-og:type', default='website')
    ogurl_handymen = models.CharField(max_length=256, verbose_name='Разнорабочие-og:url', default='http://https://xn--b1abghfcl9beiep7c.xn--p1ai/')
    ogsitename_handymen = models.CharField(max_length=256, verbose_name='Разнорабочие-og:site_name', default='ГрузчикService')

    ogtitle_dismalting = models.CharField(max_length=256, verbose_name='Демонтаж-og:title', default='ГрузчикService')
    ogdescription_dismalting = models.CharField(max_length=256, verbose_name='Демонтаж-og:description', default='Грузчики и услуги рабочих в Нижнем Новгороде')
    ogtype_dismalting = models.CharField(max_length=256, verbose_name='Демонтаж-og:type', default='website')
    ogurl_dismalting = models.CharField(max_length=256, verbose_name='Демонтаж-og:url', default='http://https://xn--b1abghfcl9beiep7c.xn--p1ai/')
    ogsitename_dismalting = models.CharField(max_length=256, verbose_name='Демонтаж-og:site_name', default='ГрузчикService')

    ogtitle_cars = models.CharField(max_length=256, verbose_name='Автотранспорт и экспедиция грузов-og:title', default='ГрузчикService')
    ogdescription_cars = models.CharField(max_length=256, verbose_name='Автотранспорт и экспедиция грузов-og:description', default='Грузчики и услуги рабочих в Нижнем Новгороде')
    ogtype_cars = models.CharField(max_length=256, verbose_name='Автотранспорт и экспедиция грузов-og:type', default='website')
    ogurl_cars = models.CharField(max_length=256, verbose_name='Автотранспорт и экспедиция грузов-og:url', default='http://https://xn--b1abghfcl9beiep7c.xn--p1ai/')
    ogsitename_cars = models.CharField(max_length=256, verbose_name='Автотранспорт и экспедиция грузов-og:site_name', default='ГрузчикService')

    ogtitle_cont = models.CharField(max_length=256, verbose_name='Контакты-og:title', default='ГрузчикService')
    ogdescription_cont = models.CharField(max_length=256, verbose_name='Контакты-og:description', default='Грузчики и услуги рабочих в Нижнем Новгороде')
    ogtype_cont = models.CharField(max_length=256, verbose_name='Контакты-og:type', default='website')
    ogurl_cont = models.CharField(max_length=256, verbose_name='Контакты-og:url', default='http://https://xn--b1abghfcl9beiep7c.xn--p1ai/')
    ogsitename_cont = models.CharField(max_length=256, verbose_name='Контакты-og:site_name', default='ГрузчикService')

    ogtitle_company = models.CharField(max_length=256, verbose_name='Компания-og:title', default='ГрузчикService')
    ogdescription_company = models.CharField(max_length=256, verbose_name='Компания-og:description', default='Грузчики и услуги рабочих в Нижнем Новгороде')
    ogtype_company = models.CharField(max_length=256, verbose_name='Компания-og:type', default='website')
    ogurl_company = models.CharField(max_length=256, verbose_name='Компания-og:url', default='http://https://xn--b1abghfcl9beiep7c.xn--p1ai/')
    ogsitename_company = models.CharField(max_length=256, verbose_name='Компания-og:site_name', default='ГрузчикService')

    ogtitle_about = models.CharField(max_length=256, verbose_name='О компании-og:title', default='ГрузчикService')
    ogdescription_about = models.CharField(max_length=256, verbose_name='О компании-og:description', default='Грузчики и услуги рабочих в Нижнем Новгороде')
    ogtype_about = models.CharField(max_length=256, verbose_name='О компании-og:type', default='website')
    ogurl_about = models.CharField(max_length=256, verbose_name='О компании-og:url', default='http://https://xn--b1abghfcl9beiep7c.xn--p1ai/')
    ogsitename_about = models.CharField(max_length=256, verbose_name='О компании-og:site_name', default='ГрузчикService')

    ogtitle_vac = models.CharField(max_length=256, verbose_name='Вакансии-og:title', default='ГрузчикService')
    ogdescription_vac = models.CharField(max_length=256, verbose_name='Вакансии-og:description', default='Грузчики и услуги рабочих в Нижнем Новгороде')
    ogtype_vac = models.CharField(max_length=256, verbose_name='Вакансии-og:type', default='website')
    ogurl_vac = models.CharField(max_length=256, verbose_name='Вакансии-og:url', default='http://https://xn--b1abghfcl9beiep7c.xn--p1ai/')
    ogsitename_vac = models.CharField(max_length=256, verbose_name='Вакансии-og:site_name', default='ГрузчикService')

    def __str__(self):
        return 'Мета теги'
    
    class Meta:
        verbose_name = 'Мета теги'
        verbose_name_plural = 'Мета теги'