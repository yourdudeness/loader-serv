# Generated by Django 2.2.5 on 2019-10-21 13:07

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0010_review_text'),
    ]

    operations = [
        migrations.AddField(
            model_name='contacts',
            name='phone2',
            field=models.CharField(default=8800, max_length=12, validators=[django.core.validators.RegexValidator(message='Введите номер в формате +00000000000', regex='^\\+\\d{11}$')], verbose_name='Второй телефон'),
            preserve_default=False,
        ),
    ]
