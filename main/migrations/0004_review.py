# Generated by Django 2.2.5 on 2019-09-04 07:37

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0003_client_thankletter'),
    ]

    operations = [
        migrations.CreateModel(
            name='Review',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('author', models.CharField(max_length=256, verbose_name='Автор')),
                ('phone', models.CharField(help_text='Последние 4 цифры скрыть *', max_length=32, verbose_name='Телефон')),
                ('city', models.CharField(max_length=256, verbose_name='Город')),
                ('rating', models.SmallIntegerField(choices=[(4, 4), (5, 5)], verbose_name='Оценка')),
            ],
            options={
                'verbose_name': 'Отзыв',
                'verbose_name_plural': 'Отзывы',
            },
        ),
    ]
