# Generated by Django 2.2.5 on 2019-10-21 17:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0013_companytext'),
    ]

    operations = [
        migrations.CreateModel(
            name='CompanyTextPrivate',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=256, verbose_name='Заголовок')),
                ('text', models.TextField(verbose_name='Текст')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
