from django.views import generic
from django.shortcuts import render
from prices.models import LoaderService, RiggingService, HandymenService, DemolitionWorkService
from django.contrib.staticfiles.storage import staticfiles_storage

from .models import Client, ThankLetter, Review, FAQ, Partner, Performer, CompanyData, CompanyText, CompanyTextPrivate, ServicesDescription


def get_min_prices():
    return {
        'loader': {
            'img_url': staticfiles_storage.url('assets/img/services/1.png'),
            'title': 'Услуги грузчиков',
            'price': LoaderService.objects.min_price()
        },
        'rigging': {
            'img_url': staticfiles_storage.url('assets/img/services/2.png'),
            'title': 'Такелажные работы',
            'price': RiggingService.objects.min_price()
        },
        'handymen': {
            'img_url': staticfiles_storage.url('assets/img/services/3.png'),
            'title': 'Услуги разнорабочих',
            'price': HandymenService.objects.min_price()
        },
        'demolition': {
            'img_url': staticfiles_storage.url('assets/img/services/4.png'),
            'title': 'Демонтаж',
            'price': DemolitionWorkService.objects.min_price()
        }
    }


class IndexView(generic.TemplateView):
    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        min_prices = get_min_prices()
        context.update({
            'loader': min_prices['loader'],
            'services': [
                min_prices['loader'], min_prices['rigging'],
                min_prices['handymen'], min_prices['demolition']
            ]
        })
        return context


class AboutView(generic.TemplateView):

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'clients': Client.objects.all(),
            'thank_letters': ThankLetter.objects.all(),
            'reviews': Review.objects.all(),
            'faq_list': FAQ.objects.all(),
            'desc_list': ServicesDescription.objects.all()
        })
        return context


class AboutCompanyView(AboutView):
    template_name = 'company.html'


class AboutCompanyPrivateView(AboutView):
    template_name = 'private_company.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'performers': Performer.objects.all(),
            'companytext': CompanyText.objects.all(),
            'companytextprivate': CompanyTextPrivate.objects.all() 
        })
        return context


class ContactsView(generic.TemplateView):
    template_name = 'contacts.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'partners': Partner.objects.all(),
            'companydata': CompanyData.objects.all()
        })
        return context

class MovingView(generic.TemplateView):
    template_name = 'moving.html'

class MoverView(generic.TemplateView):
    template_name = 'mover.html'

class HandyMenView(generic.TemplateView):
    template_name = 'handymen.html'

class DismantlingView(generic.TemplateView):
    template_name = "dismalting.html"

class ShippingView(generic.TemplateView):
    template_name = 'perevozka-gruzov.html'


def view_404(request, exception=None, template_name='404.html'):
    response = render(request, template_name, status=404)
    return response
