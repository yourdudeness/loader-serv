from django.contrib.sitemaps import Sitemap
from django.urls import reverse


class StaticViewSitemap(Sitemap):
    priority = .5
    changefreq = 'daily'

    def items(self):
        return [
            'index', 'company', 'company-private',
            'contacts', 'vacancies', 'prices',
            'vacancies', 'prices'
        ]

    def location(self, item):
        return reverse(item)
