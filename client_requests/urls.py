from django.urls import path
from . import views


urlpatterns = [
    path('ring-request/', views.RingRequestView.as_view(), name='ring_request'),
    path('vacancy-response/', views.VacancyResponseView.as_view(), name='vacancy_response'),
    path('director-mail-request/', views.DirectorMailRequestView.as_view(), name='director_mail_request'),
]
