from django.apps import AppConfig


class ClientRequestsConfig(AppConfig):
    name = 'client_requests'
    verbose_name = 'Заявки'
