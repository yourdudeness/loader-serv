from django import forms

from .models import RingRequest, VacancyResponse, DirectorMailRequest


class RingRequestForm(forms.ModelForm):
    class Meta:
        model = RingRequest
        fields = ['phone']


class VacancyResponseForm(forms.ModelForm):
    birth_date = forms.DateField(input_formats=['%d.%m.%Y'])
    citizenship = forms.ChoiceField(
        label='Гражданство', choices=VacancyResponse.CITIZENSHIP_CHOICES
    )

    class Meta:
        model = VacancyResponse
        fields = [
            'first_name', 'last_name', 'patronymic',
            'citizenship', 'birth_date', 'phone', 'vacancy',
        ]


class DirectorMailRequestForm(forms.ModelForm):
    class Meta:
        model = DirectorMailRequest
        fields = [
            'name', 'phone', 'text'
        ]
