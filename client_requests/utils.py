from django.core.mail import send_mail
from django.conf import settings


def send_ring_request_email(phone, recipients):
    mail_title = 'Заявка с сайта грузчиксервис.рф'
    mail_body = 'Новая заявка\n'
    mail_body += 'Телефон: %s\n' % phone
    send_mail(mail_title, mail_body, settings.DEFAULT_FROM_EMAIL, recipients)


def send_vacancy_response_email(first_name, last_name, patronymic, citizenship, birth_date, phone, vacancy, recipients):
    mail_title = 'Отклик на вакансию с сайта грузчиксервис.рф'
    mail_body = 'Отклик на вакансию\n'
    mail_body += 'ФИО: %s %s %s\n' % (last_name, first_name, patronymic)
    mail_body += 'Гражданство: %s\n' % citizenship
    mail_body += 'Дата рождения: %s\n' % birth_date.strftime('%Y.%m.%d')
    mail_body += 'Телефон: %s\n' % phone
    mail_body += 'Вакансия: %s\n' % vacancy
    send_mail(mail_title, mail_body, settings.DEFAULT_FROM_EMAIL, recipients)


def send_director_mail(name, phone, text, recipients):
    mail_title = 'Письмо директору с сайта грузчиксервис.рф'
    mail_body = 'Письмо директору\n'
    mail_body += 'Имя: %s\n' % name
    mail_body += 'Телефон: %s\n' % phone
    mail_body += 'Сообщение: %s\n' % text
    send_mail(mail_title, mail_body, settings.DEFAULT_FROM_EMAIL, recipients)
