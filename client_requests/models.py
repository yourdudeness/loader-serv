from django.db import models

from vacancies.models import Vacancy


class RingRequest(models.Model):
    phone = models.CharField(verbose_name='Телефон', max_length=256)
    created_at = models.DateTimeField(verbose_name='Дата создания', auto_now_add=True)

    def __str__(self):
        return self.phone

    class Meta:
        verbose_name = 'Запрос звонка'
        verbose_name_plural = 'Запросы звонков'


class VacancyResponse(models.Model):
    CITIZENSHIP_CHOICES = [
        ('UK', 'Великобритания'),
        ('Belarus', 'Белоруссия'),
        ('Russia', 'РФ'),
        ('Ukraine', 'Украина'),
        ('Israel', 'Израиль')
    ]

    first_name = models.CharField(verbose_name='Имя', max_length=256)
    last_name = models.CharField(verbose_name='Фамилия', max_length=256)
    patronymic = models.CharField(verbose_name='Отчество', max_length=256)
    citizenship = models.CharField(verbose_name='Гражданство', choices=CITIZENSHIP_CHOICES, max_length=32)
    birth_date = models.DateField(verbose_name='Дата рождения')
    phone = models.CharField(verbose_name='Телефон', max_length=256)
    created_at = models.DateTimeField(verbose_name='Дата отклика', auto_now_add=True)
    vacancy = models.CharField(verbose_name='Вакансия', max_length=256)

    def full_name(self):
        return '%s %s %s' % (self.last_name, self.first_name, self.patronymic)
    full_name.short_description = 'ФИО'

    def __str__(self):
        return self.full_name()

    class Meta:
        verbose_name = 'Отклик на вакансию'
        verbose_name_plural = 'Отклики на вакансию'


class DirectorMailRequest(models.Model):
    name = models.CharField(verbose_name='Имя', max_length=256)
    phone = models.CharField(verbose_name='Телефон', max_length=256)
    text = models.TextField(verbose_name='Сообщение')
    created_at = models.DateTimeField(verbose_name='Дата отправки', auto_now_add=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Письмо директору'
        verbose_name_plural = 'Письма директору'


class MailAddress(models.Model):
    email = models.EmailField(verbose_name='E-mail', unique=True)

    def __str__(self):
        return self.email

    class Meta:
        verbose_name = 'Почта для заявок'
        verbose_name_plural = 'Почты для заявок'
