from django.contrib import admin
from .models import RingRequest, VacancyResponse, MailAddress, DirectorMailRequest


class ReadOnlyModelAdmin(admin.ModelAdmin):
    def has_add_permission(self, request):
        return False

    def has_change_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


@admin.register(RingRequest)
class RingRequestAdmin(ReadOnlyModelAdmin):
    view_on_site = False
    list_display = ('phone', 'created_at')
    list_filter = ('created_at',)
    sortable_by = ('created_at',)


@admin.register(VacancyResponse)
class VacancyResponseAdmin(ReadOnlyModelAdmin):
    view_on_site = False
    list_display = ('full_name', 'citizenship', 'birth_date', 'phone', 'created_at')
    list_filter = ('created_at',)
    sortable_by = ('created_at',)


@admin.register(DirectorMailRequest)
class DirectorMailRequestAdmin(ReadOnlyModelAdmin):
    view_on_site = False
    list_display = ('name', 'phone', 'text', 'created_at')
    list_filter = ('created_at',)
    sortable_by = ('created_at',)


@admin.register(MailAddress)
class MailAddressAdmin(admin.ModelAdmin):
    view_on_site = False
