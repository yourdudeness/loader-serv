from django.views.generic import FormView
from django.http import JsonResponse
from .forms import RingRequestForm, VacancyResponseForm, DirectorMailRequestForm
from .models import MailAddress
from .utils import send_ring_request_email, send_vacancy_response_email, send_director_mail


def get_recipients():
    return [mail.email for mail in MailAddress.objects.all()]


class RingRequestView(FormView):
    http_method_names = ['post']
    form_class = RingRequestForm

    def form_valid(self, form):
        form.save()
        send_ring_request_email(
            form.cleaned_data['phone'],
            get_recipients()
        )
        return JsonResponse({
            'status': 'success'
        })

    def form_invalid(self, form):
        return JsonResponse({
            'status': 'error',
            'errors': form.errors
        })


class VacancyResponseView(FormView):
    http_method_names = ['post']
    form_class = VacancyResponseForm

    def form_valid(self, form):
        form.save()
        send_vacancy_response_email(
            form.cleaned_data['first_name'],
            form.cleaned_data['last_name'],
            form.cleaned_data['patronymic'],
            form.cleaned_data['citizenship'],
            form.cleaned_data['birth_date'],
            form.cleaned_data['phone'],
            form.cleaned_data['vacancy'],
            get_recipients()
        )
        return JsonResponse({
            'status': 'success'
        })

    def form_invalid(self, form):
        return JsonResponse({
            'status': 'error',
            'errors': form.errors
        })


class DirectorMailRequestView(FormView):
    http_method_names = ['post']
    form_class = DirectorMailRequestForm

    def form_valid(self, form):
        form.save()
        send_director_mail(
            form.cleaned_data['name'],
            form.cleaned_data['phone'],
            form.cleaned_data['text'],
            get_recipients()
        )
        return JsonResponse({
            'status': 'success'
        })

    def form_invalid(self, form):
        return JsonResponse({
            'status': 'error',
            'errors': form.errors
        })
