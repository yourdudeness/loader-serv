import os
from hashlib import md5
from datetime import datetime

from django.core.validators import RegexValidator
from django.contrib import admin


def upload_to(instance, filename):
    now = datetime.now()

    ext = os.path.splitext(filename)[1]
    timestamp = str(now.time())
    encode_filename = md5((filename + timestamp).encode('utf-8')).hexdigest() + ext

    app_path = os.path.join(instance._meta.app_label, instance._meta.model_name)
    dir_path = '%d-%d/%d' % (now.year, now.month, now.day)
    basedir = os.path.join(app_path, dir_path)

    return os.path.join(basedir, encode_filename)


validate_phone = RegexValidator(
    regex=r'^\+\d{11}$',
    message='Введите номер в формате +00000000000'
)


class MaxCountObjectsMixin(admin.ModelAdmin):
    max_objects = None

    def has_add_permission(self, request):
        num_objects = self.model.objects.count()
        return num_objects < self.max_objects
