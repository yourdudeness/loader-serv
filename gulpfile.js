var gulp = require('gulp'),
    sass = require('gulp-sass'),
    concat = require('gulp-concat'),
    autoprefixer = require('gulp-autoprefixer'),
    cleanCSS = require('gulp-clean-css'),
    terser = require('gulp-terser'),
    del = require('del'),
    imagemin = require('gulp-imagemin'),
    htmlmin = require('gulp-htmlmin'),
    imageminJpegRecompress = require('imagemin-jpeg-recompress');
    touch = require('gulp-touch-fd');


var stylesFiles = [
    './src/styles/**/*.scss'
];

var jsFiles = [
    './src/js/**/*.js',
    '!./src/js/scripts.js'
];

var imgFiles = [
    './src/img/**/*.jpg',
    './src/img/**/*.gif',
    './src/img/**/*.svg'
];

function styles() {

    return gulp.src(stylesFiles)
        .pipe(sass({
            includePaths: require('node-normalize-scss').includePaths
        }))
        .pipe(concat('styles.css'))
        .pipe(autoprefixer({
            browserslistrc: ['last 2 versions'],
            cascade: false
        }))
        .pipe(cleanCSS({
            level: 2
        }))
        .pipe(gulp.dest('./dist/assets/css'))
        .pipe(touch());
}


function scripts() {
    return gulp.src(jsFiles)
        .pipe(terser({mangle: true}))
        .pipe(concat('scripts.js'))
        .pipe(gulp.dest('./dist/assets/js'))
        .pipe(touch());
}

function minimg() {
    return gulp.src(imgFiles)
        .pipe(
            imagemin([
                imagemin.jpegtran({progressive: true}),
                imagemin.optipng({optimizationLevel: 5}),
                imageminJpegRecompress({
                    loops: 3,
                    min: 70,
                    max: 80,
                    quality: 'high'
                }),
                imagemin.svgo(),
            ]),
        )
        .pipe(gulp.dest('./dist/assets/img'))
}

function png() {
    return gulp.src('./src/img/**/*.png')
        .pipe(gulp.dest('./dist/assets/img'))
}

function minhtml() {
    return gulp.src('./src/*.html')
        .pipe(htmlmin({collapseWhitespace: true}))
        .pipe(gulp.dest('./dist'))
        .pipe(gulp.dest('./templates'));
}

function fonts() {
    return gulp.src('./src/fonts/**/*')
        .pipe(gulp.dest('./dist/assets/fonts'));
}

function clean() {
    return del(['dist/assets/css/**/*.css']);
}

function watch() {
    gulp.watch('./src/styles/**/*.scss', styles);
    gulp.watch(['./src/js/**/*.js', '!./src/js/scripts.js'], scripts);
    gulp.watch('./src/*.html', minhtml);
}

exports.styles = styles;
exports.scripts = scripts;
exports.delcss = clean;
exports.images = minimg;
exports.png = png;
exports.html = minhtml;
exports.fonts = fonts;
exports.watch = watch;
exports.build = gulp.series(clean, gulp.parallel(fonts, minhtml, styles, scripts, minimg, png));
exports.dev = gulp.series(exports.build, watch);
